/* See COPYING for copyright and license details */
#include <stdio.h>
#include <string.h>

#define ISWHITE(x) ((x) == '\n' || (x) == ' ' || (x) == '\f' || (x) == '\r' || (x) == '\v')
#define SKIPWHITE(x) do { while (ISWHITE(x) (x)++; } while 0

/* Removes API comments */

int main(int argc, char **argv) {
    char line[1024];
    FILE *fin = NULL, *fout = NULL;
    int comment = 0;
    int ret = 0;
	int not_empty = 0;

    if (argc < 3)
        return 1;

    fin = fopen(argv[1], "r");
    if (fin == NULL)
        return 1;

    fout = fopen(argv[2], "w");
    if (fout == NULL)
    {
        ret = 1;
        goto error_out;
    }
    
    while (fgets(line, sizeof line, fin)) {
        if (comment) {
            if (strstr(line, "*/"))
                comment = 0;
            continue;
        }
		const char *tmp = line;
		while (*tmp == ' ') {
			tmp++;
		}
		if (strncmp(tmp, "//", 2) == 0) {
			continue;
		}
        if (strncmp(tmp, "/*", 2) == 0) {
            if (strstr(tmp, "*/") == NULL)
                comment = 1;
            continue;
        }
		not_empty = 0;
		while(ISWHITE(*tmp)) {
			tmp++;
		}
		while(*tmp && *tmp != '\n') {
			if (ISWHITE(*tmp)) {
				fputc(' ', fout);
				tmp++;
				while(ISWHITE(*tmp)) {
					tmp++;
				}
			}
			else {
				not_empty = 1;
				fputc(*tmp, fout);
				tmp++;
			}
		}
		if (not_empty) {
			fputc(' ', fout);
		}
    }

error_out:
    if (fin != NULL)
        fclose(fin);
    if (fout != NULL)
        fclose(fout);
    return ret;
}
